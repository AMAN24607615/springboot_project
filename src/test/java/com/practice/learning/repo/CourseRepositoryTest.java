package com.practice.learning.repo;

import com.practice.learning.course.bean.Course;
import com.practice.learning.course.repository.CourseRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class CourseRepositoryTest {

    @Autowired
    private CourseRepository courseRepo;

    @Test
    void isCourseExistById() {
        //Course course = new Course(1,"Python Testing Course","Aman");
       // courseRepo.save(course);

        Boolean actualResult = courseRepo.IsCourseExistById(1);
        assertThat(actualResult).isTrue();
    }

    /*
    @AfterEach
    void tearDown(){
        System.out.println("tearing down");
        courseRepo.deleteAll();
    }

    @BeforeEach
    void setUp(){
        System.out.println("setup");
    }

     */

}