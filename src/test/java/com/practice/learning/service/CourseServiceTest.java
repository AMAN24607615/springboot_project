package com.practice.learning.service;

import com.practice.learning.course.repository.CourseRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class CourseServiceTest {

    @Mock   // for mock data
    private CourseRepository repo;

    private CourseService courseService;

    @BeforeEach
    void setUp(){
        this.courseService = new CourseService(this.repo);

    }
   /*
    @AfterEach
    void tearDown() throws Exception {
        this.autoCloseable.close();
    }
    */
    @Test
    void getAllCourses() {
        courseService.getAllCourses();
        verify(repo).findAll();
    }

}