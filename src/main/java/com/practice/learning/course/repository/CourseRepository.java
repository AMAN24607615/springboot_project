package com.practice.learning.course.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.practice.learning.course.bean.Course;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

// spring data jpa class //Entity ,ID data type
@Repository
public interface CourseRepository extends JpaRepository<Course,Long> {
	@Query("SELECT CASE WHEN COUNT(c) > 0 THEN TRUE ELSE FALSE END FROM Course c WHERE c.id = :id ")
	Boolean IsCourseExistById(Integer id);

	Course findByEmail(String email);
}
