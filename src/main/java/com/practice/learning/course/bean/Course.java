package com.practice.learning.course.bean;

import org.hibernate.validator.constraints.Length;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.*;
import java.util.Date;
import java.util.Locale;

@Entity
public class Course {

	@Id //to set the id
	@GeneratedValue  // to generate the autogenerate the id
	private long id;

	@NotBlank
	@Size(min=3, max = 1000,message = "name must me more then 3 char long")
	private String name;

	@NotNull
	@NotBlank
	@Size(min=3, max = 1000, message = "author name must be more then 3 char long")
	private String author;

	@NotNull
	private Boolean active;


	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dop;


	@NotBlank
	@Email(message = "Enter valid Email Address")
	@Pattern(regexp="[A-Za-z0-9\\-\\_\\.]+[@]logicsoft+[\\.][A-Za-z]{2,3}",
			message = "Must be Logicsoft Official Email Id")
	private String email;

	@NotNull
	private Number noc;

	@NotNull
	@Length(max = 10 ,min = 10,message = "Phone should number should be valid number")
	@Pattern(regexp="[7 8][0-9]{9}",message = "Phone should number should be valid number")
	private String phone;

	public Course() {
		// default constructor whenever we built entity
	}
	@Override
	public String toString() {
		return "Course [id=" + id + ", name=" + name + ", author=" + author +
				", active="+ active +", dop"+ dop + ", email"+ email + ", noc"+ noc + ", phone"+ phone +  "]";
	}


	public long getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getAuthor() {return author;}
	public Boolean getActive() {
		return active;
	}
	public Date getDop() {
		return dop;
	}
	public String getEmail() {
		return email;
	}
	public Number getNoc() {
		return noc;
	}
	public String getPhone() {
		return phone;
	}

	// setter methods
	public void setId(long id ) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setAuthor(String author) { this.author = author;}
	public void setActive(boolean active) {
		this.active = active;
	}
	public void setDop(Date dop) {
		this.dop = dop;
	}
	public void setEmail(String email) {this.email = email;}
	public void setNoc(Number noc) {this.noc = noc;}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Course(long id,String name,String author,Boolean active,Date dop,Number noc,String email,String phone) {
		super();
		this.id = id;
		this.name=name;
		this.author=author;
		this.active=active;
		this.dop=dop;
		this.noc=noc;
		this.email = email;
		this.phone = phone;
	}

	@Bean
	public MessageSource messageSource(){
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("/home/aman/Desktop/springboot_project/src/main/resources/message");
		messageSource.setDefaultEncoding("UTF-8");
		return messageSource;
	}

	@Bean
	public LocalValidatorFactoryBean validatorFactoryBean(MessageSource messageSource){
		LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
		bean.setValidationMessageSource(messageSource);
		return bean;
	}

	@Bean
	public SessionLocaleResolver localResolver(){
		SessionLocaleResolver localeResolver = new SessionLocaleResolver();
		localeResolver.setDefaultLocale(Locale.US);
		return  localeResolver;
	}
}
