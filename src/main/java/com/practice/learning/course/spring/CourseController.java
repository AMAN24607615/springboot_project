package com.practice.learning.course.spring;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.practice.learning.course.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.config.ConfigDataResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import com.practice.learning.course.bean.Course;
import com.practice.learning.course.repository.CourseRepository;

import javax.validation.Valid;
import javax.validation.constraints.Null;

//step 1
@RestController
@CrossOrigin(origins = "http://localhost:4200/")
public class CourseController {
	
	@Autowired
	private CourseRepository repository;

	Map<String,String> errors;
	//step 2
	@GetMapping("/courses")
	public List<Course>getAllCourses(){
		//return List.of(new Course(1,"Learn Microservices","in28minutes"),
		//		new Course(2,"Learn Spring","in28minutes"));
		return repository.findAll();
	}
	//Get-Retrieve Information(/courses,/courses/1)
	// display a particular course with id 
	@GetMapping("/courses/{id}")
	public Course getCoursesDetails(@PathVariable long id){
		Optional<Course> course =  repository.findById(id);
		if(course.isEmpty()) {
			throw new RuntimeException("Course not found with id "+ id);
		}
		return course.get();
	}
	//Post- Create a new course(/course)
	@PostMapping("/courses")
	public ResponseEntity<Object> createCourse(@Valid @RequestBody Course course, BindingResult bindingResult){
		if(bindingResult.hasErrors()){
			errors = new HashMap<>();
			for(FieldError error:bindingResult.getFieldErrors()){
				errors.put(error.getField(),error.getDefaultMessage());
			}
			return new ResponseEntity<>(errors, HttpStatus.NOT_ACCEPTABLE);
		}
		Course c = repository.findByEmail(course.getEmail());
		if(c!= null){
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}
		//repository.save(course);
		return new ResponseEntity<>(repository.save(course), HttpStatus.OK);

	}
	//PUT- update /replace a resourse(/course/1)
	@PutMapping("/courses/{id}")
	public  ResponseEntity<Course> updateCourse(@PathVariable long id, @RequestBody Course courseDetils) {
	    Course course = repository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Course not exist with id :"+id));

		course.setName(courseDetils.getName());
		course.setAuthor(courseDetils.getAuthor());
		course.setDop(courseDetils.getDop());
		course.setEmail(courseDetils.getEmail());
		course.setNoc(courseDetils.getNoc());
		course.setPhone(courseDetils.getPhone());
		//repository.save(course);
		Course updatecourse = repository.save(course);
		return ResponseEntity.ok(updatecourse);


	}
	//DELETE- delete a resourse (/course/1)
	@DeleteMapping("/courses/{id}")
	public ResponseEntity<Map<String,Boolean>> deleteCourse(@PathVariable long id) {
		Course course = repository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Course not exist with id:"+ id));
		repository.delete(course);
		Map<String,Boolean> response = new HashMap<>();
		response.put("delete",Boolean.TRUE);
		return ResponseEntity.ok(response);
	}
}
