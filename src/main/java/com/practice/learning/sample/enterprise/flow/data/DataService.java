package com.practice.learning.sample.enterprise.flow.data;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

//getting data
@Component
public class DataService{
   public List<Integer> retrieveData(){
		//ArrayList array = new ArrayList;
		List<Integer> arraylist  = new ArrayList<Integer>();
		arraylist.add(1);
		arraylist.add(3);
		arraylist.add(5);
		arraylist.add(100);
		return arraylist;
	}
	
}