package com.practice.learning.user.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.*;
import java.util.Date;

@Entity(name = "Users") // postgresql is not allowing me to use 'user' keyword bcs it is reserved i think
public class User {

    @Id //to set the id
    @GeneratedValue(strategy = GenerationType.IDENTITY)  // to generate the autogenerate the id
    private Long id;

    @NotBlank(message = "Name can't be null")
    @Size(min = 3,max = 10)
    private String name;

    @NotNull(message = "Email can't be null")
    @Email(message = "must be a valid email")
    private String email;

    @NotNull(message = "DOB must be in format yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dob;

    @Size(min = 10,max = 10,message = "Phone number must  be valid")
    @NotNull(message = "must not be nulls")
    private String phone;

    @Size(min = 5 ,max = 20,message = "Address should be valid")
    @NotNull(message = "must not be nulls")
    private String address;

    @NotNull(message = "Gender can't be null")
    private String gender;

    @NotNull(message = "Password can't be null")
    private String password;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", dob=" + dob +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                ", gender='" + gender + '\'' +
                ", password='" + password + '\'' +
                '}';
    }


    public User() {
    }

    public User(Long id, String name, String email, Date dob, String phone, String address, String gender,String password) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.dob = dob;
        this.phone = phone;
        this.address = address;
        this.gender = gender;
        this.password = password;
    }


    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
