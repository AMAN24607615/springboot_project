package com.practice.learning.user.controller;
import com.practice.learning.user.model.User;
import com.practice.learning.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@RestController
@CrossOrigin(origins = "http://localhost:4200/")
//@RequestMapping("/api/")
public class userController {

    @Autowired
    private UserRepository repository;

    Map<String, String> errors;
    // get all the user list
    @GetMapping("/users")
    public List<User>getAllUsers(){
        return repository.findAll();
    }

    @GetMapping("/user/{id}")
    public User getUserDatails(@PathVariable long id){
        Optional<User> user = repository.findById(id);
        if(user.isEmpty()){
            throw new RuntimeException("User not found with id "+ id);
        }
        return  user.get();
    }

    @PostMapping("/users")
    public ResponseEntity<Object> createUser(@Valid @RequestBody User user, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            errors = new HashMap<>();
            for(FieldError error: bindingResult.getFieldErrors()){
                errors.put(error.getField(),error.getDefaultMessage());
            }
            return  new ResponseEntity<>(errors, HttpStatus.NOT_ACCEPTABLE);
        }
        User u = repository.findByEmail(user.getEmail());
        if(u!=null){
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(repository.save(user),HttpStatus.OK);

    }
    // login functionality
    @PostMapping("/login")
    public ResponseEntity<Object> userLogin(@RequestBody User userData){
        System.out.println(userData);
        User user = repository.findByEmail(userData.getEmail());
        if(user.getPassword().equals(userData.getPassword())){
            return ResponseEntity.ok(user);
        }
        return (ResponseEntity<Object>) ResponseEntity.internalServerError();
    }

    @PutMapping("/users/{id}")
    public void updateUser(@PathVariable long id,@RequestBody User user){
        repository.save(user);
    }

    @DeleteMapping("/users/{id}")
    public void deleteUser(@PathVariable long id){
        repository.deleteById(id);
    }

}
