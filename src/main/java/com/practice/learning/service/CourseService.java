package com.practice.learning.service;

import com.practice.learning.course.bean.Course;
import com.practice.learning.course.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class CourseService {

    @Autowired
    private CourseRepository repo;

    public List<Course> getAllCourses(){
        return this.repo.findAll();
    }

    public CourseService(CourseRepository repo) {
        this.repo = repo;
    }
}
