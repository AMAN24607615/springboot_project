package practice;

import java.util.BitSet;
import java.util.Enumeration;
import java.util.Vector;

public class Datastructures {
    public static void main(String[] args) {
        //Enumeration
        Enumeration days; // object of enumeration type
        Vector daysName =  new Vector();
        daysName.add("Sunday");
        daysName.add("Monday");
        daysName.add("Tuesday");
        daysName.add("Wednesday");
        daysName.add("Thursday");
        daysName.add("Friday");
        daysName.add("Saturday");

        days = daysName.elements();
        while(days.hasMoreElements()) {
            System.out.println(days.nextElement());
        }

        // Bitset
        BitSet bits1  = new BitSet(16);
        BitSet bits2  = new BitSet(16);

        for(int i=0;i<16;i++){
            if(i%2==0){
                bits1.set(i);
            }
            if(i%5==0){
                bits2.set(i);
            }
        }
        System.out.println("InitialPattern in Bits1: "+bits1);
        System.out.println("InitialPattern in Bits2: "+bits2);

        //OR bits
        bits1.or(bits2);
        System.out.println("Bits1 Or Bits2:" + bits1);


        //And Bits
        bits1.and(bits2);
        System.out.println("Bits1 And Bits2:" + bits1);

        //XOR Bits
        bits1.xor(bits2);
        System.out.println("Bits1 XOR Bits2:" + bits1);




    }
}
