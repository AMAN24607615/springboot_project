# springboot_project

This is a Java Spring Boot Project which containing backend implementation with integration with Postgresql data base and Rest Api for CRUD(CREAT ,READ ,UPDATE and DELETE).

## Create Spring Boot Project selected Specification 
    1. Type : Maven Project
    2. Package : Jar
    3. Java Version : 11
    4. Language : Java
    5. Spring web
    6. Data Base : Postgresql
    7. Operating System : Ubuntu


## Rest Api and Related Code:
Packages and files are  Needed 
# 1. Model : Course
```
package com.practice.learning.course.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Course {
	@Id //to set the id 
	@GeneratedValue  // to generate the autogenerate the id
	private long id;
	
	private String name;
	private String author;
	
	public Course() {
		// default constructor whenever we built entity
	}
	@Override
	public String toString() {
		return "Course [id=" + id + ", name=" + name + ", author=" + author + "]";
	}
	
	public long getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getAuthor() {
		return author;
	}
	public Course(long id,String name,String author) {
		super();
		this.id = id;
		this.name=name;
		this.author=author;
	}
	
}

```
# 2. Repository : CourseRepository (for use of Data JPA with data base)
```
public interface CourseRepository extends JpaRepository<Course,Long> {
	Boolean IsCourseExistById(Integer id);
}
```
 2.1 : Commmand for create role in postgresql
```
 create role user_name  with createdb login password 'root';
```
# 3. Controller : CourseController
```
@RestController
public class CourseController {
	
	@Autowired
	private CourseRepository repository;

	@GetMapping("/courses")
	public List<Course>getAllCourses(){
		return repository.findAll();
	}

	@GetMapping("/courses/{id}")
	public Course getCoursesDetails(@PathVariable long id){
		Optional<Course> course =  repository.findById(id);
		if(course.isEmpty()) {
			throw new RuntimeException("Course not found with id "+ id);
		}
		return course.get();
	}
	
	@PostMapping("/courses")
	public void createCourse(@RequestBody Course course){
		repository.save(course);
	}
	
	@PutMapping("/courses/{id}")
	public void updateCourse(@PathVariable long id,@RequestBody Course course) {
		repository.save(course);
	}
	
	@DeleteMapping("/courses/{id}")
	public void deleteCourse(@PathVariable long id) {
		repository.deleteById(id);
	}
}

```
# 4. Data Base Connection in file "application.propertie"
```
#spring.datasource.url=jdbc:h2:mem:testdb
spring.jpa.defer-datasource-initialization=true

spring.datasource.url=jdbc:postgresql://localhost:5432/office
spring.datasource.username=office
spring.datasource.password=root


spring.jpa.hibernate.ddl-auto=update
spring.jpa.show-sql=true
spring.jpa.properties.hibernate.format_sql=true
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.PostgreSQL81Dialect
```
# 5. Test Case : Test of RestApi

```
@SpringBootTest
class CourseRepositoryTest {

    @Autowired
    private CourseRepository courseRepo;

    @Test
    void isCourseExistById() {
        Course course = new Course(123,"Junit Testing Course","Aman Chouhan");
        courseRepo.save(course);

        Boolean actualResult = courseRepo.IsCourseExistById(123);

        assertThat(actualResult).isTrue();
    }
}
```
# Test for Service course
```
@ExtendWith(MockitoExtension.class)
class CourseServiceTest {

    @Mock   // for mock data
    private CourseRepository repo;

    private CourseService courseService;

    @BeforeEach
    void setUp(){
        this.courseService = new CourseService(this.repo);

    }
   
    @Test
    void getAllCourses() {
        courseService.getAllCourses();
        verify(repo).findAll();
    }

}
```
# How To Run on other Machine:
```
1. First open the cmd/terminal  type : git clone https://gitlab.com/AMAN24607615/springboot_project.git
2. create role by typing query in postgresql : create role user_name  with createdb login password 'root';
3. then make some modification in application.properties file.
4. Now your project is ready to run.
```